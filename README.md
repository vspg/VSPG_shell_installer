# VSPG Shell Installer

This is the simple straigt forward exemple installer for VSPG.
The installer will :
* fetch all the requirement.
* open the firewall (on the default port 8443).
* create and launch the service.


## Installation

You can use the fearless method 

```bash
curl https://framagit.org/vspg/VSPG_shell_installer/raw/master/vspg-install-centos.sh | sudo bash
```

or the more careful way

```bash
git clone git@framagit.org:vspg/VSPG_shell_installer.git
cd shell_installer
chmod +x vspg-install-centos.sh
./vspg-install-centos.sh
```

## Requirements

- python


## Usage

Start the service
```bash
sudo systemctl start vspg_core.service
```

Stop the service
```bash
sudo systemctl stop vspg_core.service
```

Get the service status
```bash
sudo systemctl status vspg_core.service
```


