#!/bin/bash
# setup firewall rule
sudo firewall-cmd --zone=public --add-port=8443/tcp --permanent
sudo firewall-cmd --reload


# install system prerequisites
sudo yum install git wget -y


# install python prerequisites
[ ! -f get-pip.py ] && wget https://bootstrap.pypa.io/get-pip.py
sudo python get-pip.py
sudo pip install bottle
sudo pip install paste
sudo pip install requests
sudo pip install urllib3

# clone the project
git clone https://framagit.org/vspg/vspg_core.git

# find working dir
cd vspg_core
WORKINGDIR=$(pwd)

cat <<EOF > $WORKINGDIR/vspg_core.service
[Unit]
Description=Very Simple Password Generator Service
After=multi-user.target

[Service]
Type=idle
WorkingDirectory=$WORKINGDIR
ExecStart=/usr/bin/python $WORKINGDIR/vspg-api.py

[Install]
WantedBy=multi-user.target
EOF

# copie service file
sudo cp vspg_core.service /etc/systemd/system

# enable service
sudo systemctl enable vspg_core.service

#start service
sudo systemctl start vspg_core.service